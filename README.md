# Welcome to Weather API Automation Test Project

This is a simple Automation Java project using to test the API of getting current weather in specific country, And it uses "Weatherstack" API
for more info about the API please visit [Weatherstack](https://weatherstack.com/?utm_source=any-api&utm_medium=OwnedSites).

## How to install 
***Before you start make sure that you have already installed the following on your machine:***
 1. [Java](https://www.java.com/en/download/manual.jsp) JDK.
 2. [Apache maven](https://maven.apache.org/).
 3. IDE as [**_intellij_**](https://www.jetbrains.com/idea/) or [**_Eclipse_**](https://www.eclipse.org/).
 
***Steps to run the project:***

 1. Click on "Clone" button and copy the link.
 2. Open Intellij IDE and create new project from source control.
 3. Paste the URL which copied and set the path which you want to add your cloned project.
 4. Wait until all dependencies are finishing the setup.
 5. In the project panel click on "Task with serenity" folder, Then open the path "src/test/java/featuresRunner".
 6. Run "AcceptanceTests" class.

***Steps to generate HTML Report:***

 1. Open terminal into your project or in the same path
 2. Write the command "mvn clean verify" and wait until it finish.
 3. Open target folder then open the path "site/index.html".
 4. Right click on index.html then choose open in >Browser>Choose any browser.

now serenity bdd report will be opened on your browser successfully.

## How to add new tests  
The project is divided into **Four** main folders

 - Data
 - Resources
 - Features runner
 - Serenity step library 
1. First folder have specifications class which you can specify your base url and it's parameters 
2. Second folder contains the Feature files with Gherkin language so you can add the new features in this folder.
3. Third folder contains Step definition classes of the feature so you can add your steps which related to specific feature here.
4. Last folder contains the library of the steps, It's an implementation of all the steps of your step definition file.

After Adding the new test you can control the running features in the Acceptance tests class and add specific feature path in the "features="src/test/resources/features".

**Steps :**

 -  Add specification.
 - Add Feature file.
 -  Add Step Definition class.
 - Add Your steps implementation in the step library.
 - Control features which you run from Acceptance test class.
## Technologies used in the project

  Serenity Framework integrated with 
  
 - Cucumber 6
 - Rest Assured 
 - Maven
 - Junit
 - Serenity html reports
