Feature: Weather API Validation Feature

  Scenario Outline: Check if you can get current weather of existed country
    Given user choose CurrentWeatherAPI with "<accessKey>" and "<country>"
    When user calls GetCurrentWeatherAPI with Get http request
    Then API call will success with status code 200
    And "<name>" "<type>" "<region>" and "<timezone_id>" will be displayed in response body

    Examples:
      |accessKey|country|name|type|region|timezone_id|
      |c63313ea72e954eb2f8a1e3f0e4ea21b|Cairo |Cairo|City|Al Qahirah|Africa/Cairo|

    Scenario Outline: Verify that you can't find currentWeather with invalid country
      Given user choose CurrentWeatherAPI with "<accessKey>" and "<country>"
      When user calls GetCurrentWeatherAPI with Get http request
      Then API call will success with status code 200
      And API response body contains invalid country error details

      Examples:
        |accessKey|country |
        |c63313ea72e954eb2f8a1e3f0e4ea21b |invalidCountry |

      Scenario Outline: Verify that you can't access current weather with invalid access key
        Given user choose CurrentWeatherAPI with "<accessKey>" and "<country>"
        When user calls GetCurrentWeatherAPI with Get http request
        Then API call will success with status code 200
        And API response body contains "<invalidAccess>" invalid key error details

        Examples:
          |accessKey|country|invalidAccess|
          |invalidAccessKey |Cairo|invalid_access_key|

        Scenario Outline: Verify that you can't access current weather without access key
          Given user choose CurrentWeatherAPI with "<accessKey>" and "<country>"
          When user calls GetCurrentWeatherAPI with Get http request
          Then API call will success with status code 200
          And API response body contains "<invalidAccess>" missing key error details

          Examples:
            |accessKey|country|invalidAccess|
            ||Cairo|missing_access_key|

  Scenario Outline: Verify that you can't access current weather without country
    Given user choose CurrentWeatherAPI with "<accessKey>" and "<country>"
    When user calls GetCurrentWeatherAPI with Get http request
    Then API call will success with status code 200
    And API response body contains "<invalidAccess>" missing query error details

    Examples:
      |accessKey|country|invalidAccess|
      |c63313ea72e954eb2f8a1e3f0e4ea21b||missing_query|

