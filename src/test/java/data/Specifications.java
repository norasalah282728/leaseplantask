package data;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;

public class Specifications {
    public static RequestSpecification req;

    public static RequestSpecification requestSpecification(String accessKeyParam,String query)
    {
        req=new RequestSpecBuilder()
                .setBaseUri("http://api.weatherstack.com/current")
                .addParam("access_key",accessKeyParam)
                .addParam("query",query)
                .build();
        return req;
    }
}
