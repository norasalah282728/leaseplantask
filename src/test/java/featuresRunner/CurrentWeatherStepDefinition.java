package featuresRunner;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

import net.thucydides.core.annotations.Steps;
import serenityStepLibraries.CurrentWeatherSteps;

public class CurrentWeatherStepDefinition {

    @Steps
    CurrentWeatherSteps weather;

    @Given("user choose CurrentWeatherAPI with {string} and {string}")
    public void chooseCurrentWeather(String accessKey,String country){
        weather.userChooseCurrentWeatherAPIWithValidAccessKeyAndValidCountry(accessKey,country);
    }

    @When("user calls GetCurrentWeatherAPI with Get http request")
    public void callCurrentWeatherAPI(){
        weather.userCallsGetCurrentWeatherAPIWithGetHttpRequest();
    }

    @Then("API call will success with status code 200")
    public void checkStatusCode(){
        weather.api_call_will_success_with_status_code_200();
    }

    @And("{string} {string} {string} and {string} will be displayed in response body")
    public void checkBodyResponseData(String name, String type, String region,String timezone_id){
        weather.name_type_region_and_timezone_id_will_be_displayed_in_the_response_body(name,type,region,timezone_id);
    }

    @And("API response body contains invalid country error details")
    public void checkInvalidCountryErrorDetails(){
        weather.apiResponseBodyContainsInvalidCountryErrorDetails();
    }

    @And("API response body contains {string} invalid key error details")
    public void checkInvalidKeyErrorDetails(String InvalidAccessKeyError){
        weather.apiResponseBodyContainsErrorDetails(InvalidAccessKeyError);
    }

    @And("API response body contains {string} missing key error details")
    public void checkMissingAccessKeyErrorDetails(String missingAccessKeyError){
        weather.apiResponseBodyContainsMissingAccessKeyErrorDetails(missingAccessKeyError);
    }

    @And("API response body contains {string} missing query error details")
    public void checkMissingQueryErrorDetails(String missingQueryError){
        weather.apiResponseBodyContainsMissingQueryErrorDetails(missingQueryError);
    }
}
