package serenityStepLibraries;

import data.Specifications;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;
import org.hamcrest.Matchers;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class CurrentWeatherSteps {
    RequestSpecification res;
    Response response;
    JsonPath postJS;

    @Step
    public void userChooseCurrentWeatherAPIWithValidAccessKeyAndValidCountry(String accessKey, String country) {
        res = given().log().all().spec(Specifications.requestSpecification(accessKey, country));
    }

    @Step
    public void userCallsGetCurrentWeatherAPIWithGetHttpRequest() {
        response = res.when().log().all().get();
    }

    @Step
    public void api_call_will_success_with_status_code_200() {
        assertEquals(200, response.getStatusCode());
    }

    @Step
    public void name_type_region_and_timezone_id_will_be_displayed_in_the_response_body(String name, String type, String region, String timezone_id) {
        postJS = new JsonPath(response.asString());
        assertEquals(name, postJS.getJsonObject("location.name"));
        assertEquals(type, postJS.getString("request.type"));
        assertEquals(region, postJS.getString("location.region"));
        assertEquals(timezone_id, postJS.getString("location.timezone_id"));
    }

    @Step
    public void apiResponseBodyContainsInvalidCountryErrorDetails() {
        response.then()
                .log()
                .all()
                .assertThat()
                .body(Matchers.equalTo("{\"success\":false,\"error\"" +
                        ":{\"code\":615,\"type\":\"request_failed\",\"info\":" +
                        "\"Your API request failed. Please try again or contact support.\"}}"));
    }

    @Step
    public void apiResponseBodyContainsErrorDetails(String InvalidAccessKeyError) {
        postJS = new JsonPath(response.asString());
        assertEquals(InvalidAccessKeyError, postJS.getJsonObject("error.type"));
    }

    @Step
    public void apiResponseBodyContainsMissingAccessKeyErrorDetails(String missingAccessKeyError) {
        postJS = new JsonPath(response.asString());
        assertEquals(missingAccessKeyError, postJS.getJsonObject("error.type"));
    }

    @Step
    public void apiResponseBodyContainsMissingQueryErrorDetails(String missingQueryError) {
        postJS = new JsonPath(response.asString());
        assertEquals(missingQueryError, postJS.getJsonObject("error.type"));

    }

}
